import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle(({ theme }) => `
  body {
    background-color: ${theme.colors.body};
    margin: 0;
    padding: 0;
    font-family: 'Roboto', sans-serif;
    color: ${theme.colors.text.lvl1};
    box-sizing: border-box;
  }
  #root {
    position: absolute;
    box-sizing: border-box;
    width: 100%;
    height: 100%;
  }
  h1,h2,h3,h4,h5,h6,p { margin: 0 }
  @media(max-width: 600px) {
    h1 {
      font-size: 1.5em;
    }
  };
  *, *:before, *:after {
    box-sizing: border-box;
  }
`)
export default GlobalStyle
