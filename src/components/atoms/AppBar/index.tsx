import styled from 'styled-components'

const AppBar = styled.header(({ theme }) => `
  display: flex;
  align-items: center;
  padding: 8px 0;
  color: ${theme.colors.text.lvl9};
  background-color: ${theme.colors.shared.primary};
`)

export default AppBar
