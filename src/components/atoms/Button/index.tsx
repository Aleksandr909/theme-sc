import styled from 'styled-components'
import type { ButtonHTMLAttributes } from 'react'

interface StyledProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  fullWidth?: boolean;
  color?: 'primary' | 'secondary' | 'default'
}

const Button = styled.button<StyledProps>(({ fullWidth, color = 'default', theme }) => `
  color: ${theme.colors.text.lvl9};
  width: ${fullWidth ? '100%' : 'fit-content'};
  border: 1px solid rgba(0,0,0,0);
  padding: 12px 24px;
  border-radius: 8px;
  opacity: 1;
  outline: 0;
  text-transform: uppercase;
  transition: all .5s ease;
  &:not(:disabled) {
    background: ${theme.colors.shared[color]};
    cursor: pointer;
    &:hover {
      opacity: 0.8;
    }
  }
  &:disabled {
    background: ${theme.colors.shared.disabled};
  }
`)

export interface Props extends StyledProps {
  loading?: boolean;
}

export default Button
