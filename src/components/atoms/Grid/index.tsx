import styled from 'styled-components'

export interface IGridProps {
    container?: boolean;
    justify?: 'center' | 'flex-end' | 'flex-start' | 'space-between';
    alignItems?: 'center' | 'flex-end' | 'flex-start';
    nowrap?: boolean;
    fullWidth?: boolean;
    fullHeight?: boolean;
    direction?: 'column' | 'row';
    gap?: number;
    flex?: number;
}

const Grid = styled.div<IGridProps>((props) => `
    width: ${props.fullWidth ? '100%' : 'auto'};
    height: ${props.fullHeight ? '100%' : 'auto'};
    ${props.container ? 'display: flex' : ''};
    ${props.container && props.gap ? `
        margin: -${props.gap * 8}px; 
        width: calc(100% + ${props.gap * 16}px); 
        & > * {margin: ${props.gap * 8}px};
    ` : ''}
    flex-wrap: ${props.nowrap ? 'nowrap' : 'wrap'};
    ${props.justify ? `justify-content:${props.justify};` : ''}
    ${props.alignItems ? `align-items:${props.alignItems};` : ''}
    ${props.direction ? `flex-direction:${props.direction};` : ''}
    ${props.flex ? `flex:${props.flex};` : ''}
`)

export default Grid
