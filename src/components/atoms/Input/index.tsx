/* eslint-disable max-len */
import styled from 'styled-components'
import React, { InputHTMLAttributes } from 'react'

interface InputContainerProps {
  fullWidth?: boolean
}

const InputContainer = styled.div<InputContainerProps>(({ theme, fullWidth }) => `
  position: relative;
  width: ${fullWidth ? '100%' : 'fit-content'};
  & label {
    font-size: 0.875em;
    margin-left: 2px;
    color: ${theme.colors.text.lvl3};
  }
  & input{
    box-sizing: border-box;
    width: ${fullWidth ? '100%' : 'fit-content'};
    border: 0;
    outline:0;
    padding-bottom: 8px; 
    border-bottom: 1px solid ${theme.colors.text.lvl5};
    background: rgba(0,0,0,0);
    color: ${theme.colors.text.lvl1};
    & + span {
      content: '';
      position: absolute; 
      bottom: -0.5px; 
      left: 50%; 
      width: 0; 
      height: 2px;
      background-color: ${theme.colors.shared.primary}; 
      transition: 0.4s;
      border-radius: 2px;
    }
    &:focus + span {
        width: 100%;
        left: 0;
    }
    &:invalid + span {
        background-color: ${theme.colors.shared.error}; 
    }
  }
`)

export interface IInputProps extends InputContainerProps, InputHTMLAttributes<HTMLInputElement>{
  inputRef?: React.MutableRefObject<HTMLInputElement>
  label?: string
}

const Input:React.FC<IInputProps> = ({
  id, label, fullWidth, inputRef, ...props
}) => (
  <InputContainer fullWidth={fullWidth}>
    {label && <label htmlFor={id}>{label}</label>}
    <input id={id} {...props} ref={inputRef} />
    <span />
  </InputContainer>
)

export default Input
