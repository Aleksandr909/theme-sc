import styled from 'styled-components'

const Form = styled.form(({ theme }) => `
  display: flex;
  flex-direction: column;
  gap: 16px;
  width: 100%;
  max-width: 400px;
  border: 1px solid ${theme.colors.text.lvl3};
  border-radius: 8px;
  padding: 16px;
`)

export default Form
