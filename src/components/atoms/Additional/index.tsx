import styled from 'styled-components'
import Grid from '../Grid'
import { ReactComponent as HabrLogo } from '../../../assets/img/habr.svg'
import { ReactComponent as GitLogo } from '../../../assets/img/gitlab-logo-1-color-black-rgb.svg'

const AdditionalContainer = styled(Grid)(({ theme }) => `
    position: fixed;
    right: 0;
    bottom: 0;
    padding: 8px 16px;
    color: ${theme.colors.text.lvl1};
    fill: ${theme.colors.text.lvl1};
`)

const Additional: React.FC = () => (
  <AdditionalContainer container alignItems="center" gap={1} justify="flex-end">
    <a href="https://habr.com/ru/post/563572/" target="_blank" rel="noopener noreferrer" aria-label="Habr post">
      <HabrLogo height={20} />
    </a>
    <a href="https://gitlab.com/Aleksandr909/theme-sc" target="_blank" rel="noopener noreferrer" aria-label="Gitlab">
      <GitLogo height={40} />
    </a>
  </AdditionalContainer>
)

export default Additional
