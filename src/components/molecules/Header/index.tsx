import { useContext } from 'react'
import Grid from '../../atoms/Grid'
import Container from '../../atoms/Conrainer'
import ButtonStyled from '../../atoms/Button'
import AppBar from '../../atoms/AppBar'
import ThemeContext from '../../../contexts/ThemeContext'

const Header: React.FC = () => {
  const { theme, onChangeTheme } = useContext(ThemeContext)
  return (
    <AppBar>
      <Container>
        <Grid container alignItems="center" justify="space-between" gap={1}>
          <h1>
            Themization
          </h1>
          <ButtonStyled color="secondary" onClick={() => onChangeTheme(theme === 'light' ? 'dark' : 'light')}>
            set theme
          </ButtonStyled>
        </Grid>
      </Container>
    </AppBar>
  )
}

export default Header
