import Input from '../../atoms/Input'
import Form from '../../atoms/Form'
import ButtonStyled from '../../atoms/Button'

const SignUpForm: React.FC = () => {
  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const formData = new FormData(e.currentTarget)
    const data = Object.fromEntries(formData.entries())
    // eslint-disable-next-line no-alert
    window.alert(`Good job, ${data.name} ${data.surname}`)
  }
  return (
    <Form onSubmit={onSubmit}>
      <Input
        name="name"
        placeholder="Name *"
        required
        fullWidth
      />
      <Input
        name="surname"
        placeholder="Surname *"
        required
        fullWidth
      />
      <Input
        name="email"
        placeholder="Email"
        type="email"
        fullWidth
      />
      <Input
        name="phone"
        placeholder="Phone"
        type="phone"
        fullWidth
      />
      <ButtonStyled color="primary" fullWidth>
        submit
      </ButtonStyled>
    </Form>
  )
}

export default SignUpForm
