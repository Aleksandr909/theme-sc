// import original module declarations
import 'styled-components'
import lightTheme from './theme/light'

type ThemeType = typeof lightTheme

// and extend them!
declare module 'styled-components' {
  export interface DefaultTheme extends ThemeType {}
}
