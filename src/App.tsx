import { ThemeProvider } from 'styled-components'
import { useEffect, useState } from 'react'
import themes from './theme'
import ThemeContext from './contexts/ThemeContext'
import Form from './components/molecules/SignUpForm'
import Header from './components/molecules/Header'
import GlobalStyle from './components/GlobalStyle'
import Grid from './components/atoms/Grid'
import Container from './components/atoms/Conrainer'
import Additional from './components/atoms/Additional'

function App(): JSX.Element {
  const [theme, setTheme] = useState<'light' | 'dark'>('light')
  const [isLoaded, setIsLoaded] = useState(false)
  const onChangeTheme = (newTheme: 'light' | 'dark') => {
    localStorage.setItem('theme', newTheme)
    setTheme(newTheme)
  }
  useEffect(() => {
    const savedTheme = localStorage?.getItem('theme') as 'light' | 'dark' | null
    if (savedTheme && Object.keys(themes).includes(savedTheme)) setTheme(savedTheme)
    else if (window.matchMedia?.('(prefers-color-scheme: dark)').matches) {
      onChangeTheme('dark')
    }
    setIsLoaded(true)
  }, [])
  useEffect(() => {
    window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', (e) => {
      if (e.matches) {
        onChangeTheme('dark')
      } else {
        onChangeTheme('light')
      }
    })
  }, [])

  if (!isLoaded) return <></>

  return (
    <ThemeProvider theme={themes[theme]}>
      <ThemeContext.Provider value={{ theme, onChangeTheme }}>
        <GlobalStyle />
        <Grid container direction="column" fullHeight>
          <Header />
          <Grid flex={1} container>
            <Container>
              <Grid container fullHeight alignItems="center" justify="center">
                <Form />
              </Grid>
            </Container>
          </Grid>
        </Grid>
        <Additional />
      </ThemeContext.Provider>
    </ThemeProvider>
  )
}

export default App
