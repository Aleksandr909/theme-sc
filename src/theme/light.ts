const lightTheme = {
  colors: {
    body: '#ECEFF1',
    antiBody: '#263238',
    shared: {
      primary: '#1565C0',
      secondary: '#EF6C00',
      error: '#C62828',
      default: '#9E9E9E',
      disabled: '#E0E0E0',
    },
    text: {
      lvl1: '#263238',
      lvl3: '#546E7A',
      lvl5: '#78909C',
      lvl7: '#B0BEC5',
      lvl9: '#ECEFF1',
    },

  },
}

export default lightTheme
