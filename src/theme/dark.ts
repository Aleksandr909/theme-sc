const darkTheme = {
  colors: {
    body: '#263238',
    antiBody: '#ECEFF1',
    shared: {
      primary: '#90CAF9',
      secondary: '#FFE0B2',
      error: '#FFCDD2',
      default: '#BDBDBD',
      disabled: '#616161',
    },
    text: {
      lvl1: '#ECEFF1',
      lvl3: '#B0BEC5',
      lvl5: '#78909C',
      lvl7: '#546E7A',
      lvl9: '#263238',
    },
  },
}

export default darkTheme
