# Themization. Styled-components example

[![pipeline status](https://gitlab.com/Aleksandr909/theme-sc/badges/master/pipeline.svg)](https://gitlab.com/Aleksandr909/theme-sc/-/commits/master)

* [Демо](https://theme-sc.vorfolio.ru/)

* [Статья](https://habr.com/ru/post/563572/)

## О приложении

Приложение создано с помощью инструмента [create-react-app](https://ru.reactjs.org/docs/create-a-new-react-app.html) с шаблоном typescript

```sh
npx create-react-app theme-sc --template typescript
```

Для стилей использована библиотека [styled-components](https://styled-components.com/). Для роли основной цветовой палитры выбол пал на [material](https://material.io/design/color/the-color-system.html#tools-for-picking-colors).

### Локальная разработка

```sh
git clone https://gitlab.com/Aleksandr909/theme-sc.git
cd theme-sc
yarn
yarn start
```
